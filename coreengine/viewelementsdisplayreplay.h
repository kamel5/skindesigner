#ifndef __VIEWELEMENTSDR_H
#define __VIEWELEMENTSDR_H

#include "viewelement.h"
#include "../extensions/scrapmanager.h"

// define recordingtypes
enum eRecType_t {
    NoRec = 0,
    NormalRec,
    TimeshiftRec
};

/******************************************************************
* cVeDrRecTitle
******************************************************************/
class cVeDrRecTitle : public cViewElement {
private:
    const cRecording *recording;
    const cEvent *event;
    char *title;
    eRecType_t timeShiftActive;
public:
    cVeDrRecTitle(void);
    virtual ~cVeDrRecTitle(void);
    void SetTokenContainer(void);
    void Set(const cRecording *recording = NULL, const cEvent *event = NULL, eRecType_t timeShiftActive = NoRec);
    void Set(const char *title = NULL);
    bool Parse(bool forced = false);
};

/******************************************************************
* cVeDrRecInfo
******************************************************************/
class cVeDrRecInfo : public cViewElement {
private:
    const cRecording *recording;
public:
    cVeDrRecInfo(void);
    virtual ~cVeDrRecInfo(void);
    void SetTokenContainer(void);
    void Set(const cRecording *recording);
    bool Parse(bool forced = false);
};

/******************************************************************
* cVeDrCurrentTime
******************************************************************/
class cVeDrCurrentTime : public cViewElement {
private:
    bool changed;
    char *current;
    eRecType_t timeShiftActive;
public:
    cVeDrCurrentTime(void);
    virtual ~cVeDrCurrentTime(void);
    void SetTokenContainer(void);
    void Set(const char *current, eRecType_t timeShiftActive = NoRec);
    bool Parse(bool forced = false);
};

/******************************************************************
* cVeDrTotalTime
******************************************************************/
class cVeDrTotalTime : public cViewElement {
private:
    bool changed;
    char *total;
    eRecType_t timeShiftActive;
    char *timeshiftDuration;
public:
    cVeDrTotalTime(void);
    virtual ~cVeDrTotalTime(void);
    void SetTokenContainer(void);
    void Set(const char *total, const char *timeshiftDuration = NULL, eRecType_t timeShiftActive = NoRec);
    bool Parse(bool forced = false);
};

/******************************************************************
* cVeDrTimeshiftTimes
******************************************************************/
class cVeDrTimeshiftTimes : public cViewElement {
private:
    cString start;
    cString playbacktime;
    cString timeshiftrest;
    bool changed;
    eRecType_t timeShiftActive;
public:
    cVeDrTimeshiftTimes(void);
    virtual ~cVeDrTimeshiftTimes(void);
    void SetTokenContainer(void);
    void Set(cString start, cString playbacktime, cString timeshiftrest, eRecType_t timeShiftActive = NoRec);
    bool Parse(bool forced = false);
};

/******************************************************************
* cVeDrEndTime
******************************************************************/
class cVeDrEndTime : public cViewElement {
private:
    cString end;
    eRecType_t timeShiftActive;
    bool changed;
public:
    cVeDrEndTime(void);
    virtual ~cVeDrEndTime(void);
    void SetTokenContainer(void);
    void Set(cString end, eRecType_t timeShiftActive = NoRec);
    bool Parse(bool forced = false);
};

/******************************************************************
* cVeDrProgressBar
******************************************************************/
class cVeDrProgressBar : public cViewElement {
private:
    int current;
    int total;
    eRecType_t timeShiftActive;
    int timeshiftTotal;
    bool changed;
public:
    cVeDrProgressBar(void);
    virtual ~cVeDrProgressBar(void);
    void SetTokenContainer(void);
    void Set(int current, int total, eRecType_t timeShiftActive = NoRec, int timeshiftTotal = 0);
    bool Parse(bool forced = false);
};

/******************************************************************
* cVeDrCutMarks
******************************************************************/
class cVeDrCutMarks : public cViewElement {
private:
    int cutmarksIndex;
    const cMarks *marks;
    int current;
    int total;
    eRecType_t timeShiftActive;
    int timeshiftTotal;
    int numMarksLast;
    bool changed;
public:
    cVeDrCutMarks(void);
    virtual ~cVeDrCutMarks(void);
    void SetTokenContainer(void);
    void Set(const cMarks *marks, int current, int total, eRecType_t timeShiftActive = NoRec, int timeshiftTotal = 0);
    bool Parse(bool forced = false);
};

/******************************************************************
* cVeDrControlIcons
******************************************************************/
class cVeDrControlIcons : public cViewElement {
private:
    bool play;
    bool forward;
    int speed;
    bool changed;
public:
    cVeDrControlIcons(void);
    virtual ~cVeDrControlIcons(void);
    void SetTokenContainer(void);
    void Set(bool play, bool forward, int speed);
    bool Parse(bool forced = false);
};

/******************************************************************
* cVeDrProgressModeonly
******************************************************************/
class cVeDrProgressModeonly : public cViewElement {
private:
    double fps;
    int current;
    int total;
    bool changed;
public:
    cVeDrProgressModeonly(void);
    virtual ~cVeDrProgressModeonly(void);
    void SetTokenContainer(void);
    void Set(double fps, int current, int total);
    bool Parse(bool forced = false);
};

/******************************************************************
* cVeDrJump
******************************************************************/
class cVeDrJump : public cViewElement {
private:
    char *jump;
    bool changed;
public:
    cVeDrJump(void);
    virtual ~cVeDrJump(void);
    void SetTokenContainer(void);
    void Set(const char *jump);
    bool Parse(bool forced = false);
};

/******************************************************************
* cVeDrOnPause
******************************************************************/
class cVeDrOnPause : public cViewElement, public cScrapManager {
private:
    bool started;
    int actorsIndex;
    char *recfilename;
public:
    cVeDrOnPause(void);
    virtual ~cVeDrOnPause(void);
    void Close(void);
    int Delay(void) { return attribs->Delay() * 1000; };
    void SetTokenContainer(void);
    void Set(const char *recfilename);
    bool Parse(bool forced = false);
    bool Started(void) { return started; };
    void ResetSleep(void);
};

/******************************************************************
* cVeDrScraperContent
******************************************************************/
class cVeDrScraperContent : public cViewElement, public cScrapManager {
private:
    const cRecording *recording;
public:
    cVeDrScraperContent(void);
    virtual ~cVeDrScraperContent(void);
    void SetTokenContainer(void);
    void Set(const cRecording *recording);
    bool Parse(bool forced = false);
};

#endif //__VIEWELEMENTSDR_H
