#include "osdwrapper.h"

cSdOsd::cSdOsd(void) {
    osd = NULL;
}

cSdOsd::~cSdOsd(void) {
    DeleteOsd();
}

void cSdOsd::Lock(void) {
    mutex.Lock();
}

void cSdOsd::Unlock(void) {
    mutex.Unlock();
}

bool cSdOsd::CreateOsd(int x, int y, int width, int height) {
    cOsd *newOsd = cOsdProvider::NewOsd(cOsd::OsdLeft() + x, cOsd::OsdTop() + y);
    if (newOsd) {
        tArea Area = { 0, 0, width - 1, height - 1,  32 };
        if (newOsd->SetAreas(&Area, 1) == oeOk) {
            Lock();
            osd = newOsd;
            Unlock();
            dsyslog ("skindesigner: Osd of size %ix%i pixel was created.\n", width, height);
            return true;
        }
    }
    return false;
}

void cSdOsd::DeleteOsd(void) {
    Lock();
    delete osd;
    osd = NULL;
    Unlock();
    dsyslog ("skindesigner: Osd deleted.\n");
}

cPixmap *cSdOsd::CreatePixmap(int Layer, cRect &ViewPort, cRect &DrawPort) {
    if (!osd)
        return NULL;

    if (cPixmap *pixmap = osd->CreatePixmap(Layer, ViewPort, DrawPort)) {
        return pixmap;
    }

    esyslog("skindesigner: Could not create pixmap of size %i x %i", DrawPort.Size().Width(), DrawPort.Size().Height());
    int width = std::min(DrawPort.Size().Width(), osd->MaxPixmapSize().Width());
    int height = std::min(DrawPort.Size().Height(), osd->MaxPixmapSize().Height());
    DrawPort.SetSize(width, height);
    if (cPixmap *pixmap = osd->CreatePixmap(Layer, ViewPort, DrawPort)) {
        esyslog("skindesigner: Create pixmap of reduced size %i x %i", width, height);
        return pixmap;
    }

    esyslog("skindesigner: Could not create pixmap of reduced size %i x %i", width, height);
    return NULL;
}

void cSdOsd::DestroyPixmap(cPixmap *pix) {
    if (osd) {
        osd->DestroyPixmap(pix);
    }
}

void cSdOsd::Flush(void) {
    if (osd) {
        osd->Flush();
    }
}
