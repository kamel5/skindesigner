#ifndef __WORKER_H
#define __WORKER_H

#include <vdr/plugin.h>

class cWorker: public cThread {
protected:
    virtual void Action(void);
public:
    cWorker(void);
    virtual ~cWorker(void);
    bool IsRunning(void) { return Running(); };
};
#endif //__WORKER_H
