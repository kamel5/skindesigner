#include "../config.h"
#include "worker.h"

/*************************************************************************
* cWorker
*************************************************************************/
cWorker::cWorker(): cThread("SD Worker") {
    dsyslog("skindesigner: create worker thread");
}

cWorker::~cWorker(void) {
    Cancel(-1);
    while (Active())
        cCondWait::SleepMs(10);
    dsyslog("skindesigner: destroy worker thread");
}

void cWorker::Action(void) {    
#define REFRESH_INTERVALL_MS 1000
    dsyslog("skindesigner: start worker thread");
    int w, h;
    double a;
    int mode = 0;
    cSkin *Skin;
    while (Running()) {
        if (cDevice::PrimaryDevice())
            cDevice::PrimaryDevice()->GetVideoSize(w, h, a);
        if (!w && !h && !mode) {
            mode = 1;
            config.mode_changed = true;
            dsyslog("skindesigner: w %d h %d mode changed to %d\n", w, h, mode);
        }
        else if (w && h && mode) {
            mode = 0;
            config.mode_changed = true;
            dsyslog("skindesigner: w %d h %d mode changed to %d\n", w, h, mode);
        }
        Skin = Skins.Get(Skins.Current()->Index());
        if (config.GetSkinName().compare(Skin->Name()) != 0) {
            dsyslog("skindesigner: stop worker thread");
            break;
        }
        cCondWait::SleepMs(REFRESH_INTERVALL_MS);
    }
}
